<?php
error_reporting(0);

@session_start();
if ($_SESSION['id_penumpang']){
	include 'private/lib/function.php';
	include "koneksi.php";
	$idletime =70 * 70; 

	if(isset($_SESSION["timestamp"])) {
		if (time()-$_SESSION["timestamp"]>$idletime){
			session_destroy();
			echo "<script>alert('Waktu Login Anda Telah Habis !'); window.location = 'login.php'</script>";
		}
	}else{
		$_SESSION["timestamp"]=time();
	}

//pembuatan session timestamp $_SESSION["timestamp"]=time();
	?>


<?php
include "page/header.php";
?>
		</div>
	</div>
	
	<!-- START: BOOKING TAB -->
	<div class="row booking-tab">
		<div class="container clear-padding">
			<ul class="nav nav-tabs">
				<li class=" col-md-4 col-sm-4 col-xs-4"><a data-toggle="tab" href="#review-booking" class="text-center"> <span></span></a></li>
				<li class="active col-md-4 col-sm-4 col-xs-4"><a data-toggle="tab" href="#billing-info" class="text-center"><i class="fa fa-check-square"></i> <span>Transfer</span></a></li>
				<li class="col-md-4 col-sm-4 col-xs-4"><a data-toggle="tab" href="#billing-info" class="text-center"></i> <span></span></a></li>
			</ul> 
		</div>
	</div>
	<div class="row booking-detail">
		<div class="container clear-padding">
			<div class="tab-content">
				<div id="review-booking" class="tab-pane fade">
					<div class="col-md-8 col-sm-8">
						<div class="flight-list-v2">
							<div class="flight-list-main">	
								<div class="col-md-2 col-sm-2 text-center airline">
									<img src="assets/images/airline/vistara-2x.png" alt="cruise">
									<h6>Vistara</h6>
								</div>
								<div class="col-md-3 col-sm-3 departure">
									<h3><i class="fa fa-plane"></i> LHR 19:00</h3>
									<h5 class="bold">SAT, 21 SEP</h5>
									<h5>London, UK</h5>
								</div>
								<div class="col-md-4 col-sm-4 stop-duration">
									<div class="flight-direction">
									</div>
									<div class="stop">
									</div>
									<div class="stop-box">
										<span>0 Stop</span>
									</div>
									<div class="duration text-center">
										<span><i class="fa fa-clock-o"></i> 02h 00m</span>
									</div>
								</div>
								<div class="col-md-3 col-sm-3 destination">
									<h3><i class="fa fa-plane fa-rotate-90"></i> DEL 21:00</h3>
									<h5 class="bold">SAT, 21 SEP</h5>
									<h5>New Delhi, IN</h5>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="flight-list-footer">
								<div class="col-md-6 col-sm-6 col-xs-6 sm-invisible">
									<span><i class="fa fa-plane"></i> Economy</span>
									<span class="refund"><i class="fa fa-undo"></i> Refundable</span>
									<span><i class="fa fa-suitcase"></i> 10 KG</span>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12 clear-padding">
									<div class="pull-right">
										<a href="#">Edit Flight</a>
									</div>
								</div>
							</div>
						</div>
						<div class="login-box">
							<h3>Sign In</h3>
							<div class="booking-form">
								<div class="col-md-6 col-sm-6">
									<form >
										<label>Email</label>
										<input class="form-control" type="email" name="emailid" placeholder="Enter Your Email" required>
										<label>Password</label>
										<input class="form-control" type="password" name="password" placeholder="Enter Password" required>
										<a href="#">Forget Password?</a>
										<label>Phone Number (Optional)</label>
										<input class="form-control" type="text" name="phone">
										<label><input type="checkbox" name="remember"> Remember me</label>
										<button type="submit">Login</button>
									</form>
								</div>
								<div class="col-md-6 col-sm-6 text-center">
									<div class="social-media-login">
										<a href="#"><i class="fa fa-facebook"></i>Log in With Facebook</a>
										<span>Or</span>
										<a href="#"><i class="fa fa-plus"></i>Create Account</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 booking-sidebar">
						<div class="sidebar-item">
							<h4><i class="fa fa-bookmark"></i>Fare Details</h4>
							<div class="sidebar-body">
								<table class="table">
									<tr>
										<td>Adult 1</td>
										<td>$199</td>
									</tr>
									<tr>
										<td>Base Fare</td>
										<td>$100</td>
									</tr>
									<tr>
										<td>Service Fee</td>
										<td>$50</td>
									</tr>
									<tr>
										<td>Airport Taxes</td>
										<td>$20</td>
									</tr>
									<tr>
										<td>You Pay</td>
										<td class="total">$500</td>
									</tr>
								</table>
							</div>
						</div>
				<div class="sidebar-item contact-box">
							<h4><i class="fa fa-phone"></i>Butuh Bantuan?</h4>
							<div class="sidebar-body text-center">
								<p>Butuh Bantuan ? Silahkan hubungi admin</p>
								<h2>+91 1234567890</h2>
							</div>
						</div>
					</div>
				</div>
				<div id="passenger-info" class="tab-pane fade">
					<div class="col-md-8 col-sm-8">
						<div class="passenger-detail">
							<h3>Passenger Details</h3>
							<div class="passenger-detail-body">
								<form >
									<div class="col-md-6 ol-sm-6">
										<label>First Name</label>
										<input type="text" name="firstname" required class="form-control">
									</div>
									<div class="col-md-6 ol-sm-6">
										<label>Last Name</label>
										<input type="text" name="lastname" required class="form-control">
									</div>
									<div class="col-md-6 ol-sm-6">
										<label>Email</label>
										<input type="email" name="email" required class="form-control">
									</div>
									<div class="col-md-6 ol-sm-6">
										<label>Verify Email</label>
										<input type="email" name="verify_email" class="form-control">
									</div>
									<div class="col-md-6 ol-sm-6">
										<label>Country Code</label>
										<select name="countrycode" class="form-control">
											<option>+1 United States</option>
											<option>+1 Canada</option>
											<option>+44 United Kingdom</option>
											<option>+91 India</option>
										</select>
									</div>
									<div class="col-md-6 ol-sm-6">
										<label>Phone Number</label>
										<input type="text" name="phonenumber" class="form-control" required>
									</div>
									<div class="col-md-12">
										<label><input type="checkbox" name="alert"> Send me newsletters and updates</label>
									</div>
									<div class="text-center">
										<button type="submit">CONTINUE <i class="fa fa-chevron-right"></i></button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 booking-sidebar">
						<div class="sidebar-item">
							<h4><i class="fa fa-phone"></i>Need Help?</h4>
							<div class="sidebar-body text-center">
								<p>Butuh Bantuan ? Call us or drop a message. Our agents will be in touch shortly.</p>
								<h2>+91 1234567890</h2>
							</div>
						</div>
					</div>
				</div>

				<div id="billing-info" class="tab-pane fade in active">
					<div class="col-md-8 col-sm-8">
						<div class="passenger-detail">
							<h3>Konfirmasi Pembayaran </h3>
							<div class="passenger-detail-body">
								<?php
				$id_pelanggan = $_SESSION['id_penumpang'];


							$ambildata = mysqli_query($conn, "SELECT * FROM pemesanan WHERE id_pelanggan='$id_pelanggan' AND  status='Proses' ");
							while ($id = mysqli_fetch_array($ambildata)) 
							{
								?>
								<div class="add-new-card">
									<form method="post">
										<?php
$host = "localhost";
$user = "root";
$pass = "";
$namadb = "db_tiket";

$conn = mysqli_connect($host, $user, $pass, $namadb);
if (!$conn) 
{
		die("Connection Failed : ". mysqli_connect_error() );
}
?>
<?php
    //untuk memulai session
    session_start();
     
    //set session dulu dengan nama $_SESSION["mulai"]
    if (isset($_SESSION["mulai_sekarang"])) { 
        //jika session sudah ada
        $telah_berlalu = time() - $_SESSION["mulai_sekarang"];
    } else { 
        //jika session belum ada
        $_SESSION["mulai_sekarang"]  = time();
        $telah_berlalu      = 0;
    } 
?>
<?php
    $sql    = mysqli_query($conn, "SELECT * FROM timer");   
    $data   = mysqli_fetch_array($sql);
 
    $temp_waktu = ($data['waktu']*60) - $telah_berlalu; //dijadikan detik dan dikurangi waktu yang berlalu
    $temp_menit = (int)($temp_waktu/60);                //dijadikan menit lagi
    $temp_detik = $temp_waktu%60;                       //sisa bagi untuk detik
     
    if ($temp_menit < 60) { 
        /* Apabila $temp_menit yang kurang dari 60 meni */
        $jam    = 0; 
        $menit  = $temp_menit; 
        $detik  = $temp_detik; 
    } else { 
        /* Apabila $temp_menit lebih dari 60 menit */           
        $jam    = (int)($temp_menit/60);    //$temp_menit dijadikan jam dengan dibagi 60 dan dibulatkan menjadi integer 
        $menit  = $temp_menit%60;           //$temp_menit diambil sisa bagi ($temp_menit%60) untuk menjadi menit
        $detik  = $temp_detik;
    }   
?>
<head>
    <!-- Kita membutuhkan jquery, disini saya menggunakan langsung dari jquery.com, jquery ini bisa didownload dan ditaruh dilocal -->
    <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
  
    <!-- Script Timer -->
    <script type="text/javascript">
        $(document).ready(function() {
            /** Membuat Waktu Mulai Hitung Mundur Dengan 
                * var detik;
                * var menit;
                * var jam;
            */
            var detik   = <?= $detik; ?>;
            var menit   = <?= $menit; ?>;
            var jam     = <?= $jam; ?>;
                  
            /**
               * Membuat function hitung() sebagai Penghitungan Waktu
            */
            function hitung() {
                /** setTimout(hitung, 1000) digunakan untuk 
                     * mengulang atau merefresh halaman selama 1000 (1 detik) 
                */
                setTimeout(hitung,1000);
  
                /** Jika waktu kurang dari 10 menit maka Timer akan berubah menjadi warna merah */
                if(menit < 10 && jam == 0){
                    var peringatan = 'style="color:red"';
                };
                /** Menampilkan Waktu Timer pada Tag #Timer di HTML yang tersedia */
                $('#timer').html(
                    '<h1 align="center"'+peringatan+'>Sisa waktu anda <br />' + jam + ' jam : ' + menit + ' menit : ' + detik + ' detik</h1><hr>'
                );
  
                /** Melakukan Hitung Mundur dengan Mengurangi variabel detik - 1 */
                detik --;
  
                /** Jika var detik < 0
                    * var detik akan dikembalikan ke 59
                    * Menit akan Berkurang 1
                */
                if(detik < 0) {
                    detik = 59;
                    menit --;
  
                   /** Jika menit < 0
                        * Maka menit akan dikembali ke 59
                        * Jam akan Berkurang 1
                    */
                    if(menit < 0) {
                        menit = 59;
                        jam --;
  
                        /** Jika var jam < 0
                            * clearInterval() Memberhentikan Interval dan submit secara otomatis
                        */
                             
                        if(jam < 0) { 
                            clearInterval(hitung); 
                            /** Variable yang digunakan untuk submit secara otomatis di Form */
                            var frmSoal = document.getElementById("frmSoal"); 
                            alert('Waktu Anda telah habis');
                            frmSoal.submit(); 
                        } 
                    } 
                } 
            }           
            /** Menjalankan Function Hitung Waktu Mundur */
            hitung();
        });
    </script>
</head>
<body>        
    <div id='timer'></div>
    <form action="logout.php" id="frmSoal" method='POST' > 
        <!-- Taruh script disini, bisa soal atau sesuai kebutuhan -->
    </form>
</body>
									<div class="col-md-7 col-sm-7">
										<div class="col-md-6 col-sm-6 text-center airline">

								<img style="width: 200px; margin-left: 200px;"src="assets/images/1280px-BCA_logo.svg.png" alt="cruise">
							</div>
						</div>
                                              <div class="col-md-8 ol-sm-8">
											<label style="margin-left: 250px">Total yang harus dibayar </label>
										</div>		
    <div class="col-md-10 ol-sm-10">
											<h2 style="margin-left: 220px">IDR <?php echo  $id['total_bayar']; ?></h2>
										</div>							
											  <div class="col-md-10 ol-sm-10">

											<h2 style="margin-left: 140px">Kode Pemesanan :<?php echo  $id['kode_pemesanan']; ?>  </h2>
										</div>							
											
										 <div class="col-md-10 ol-sm-10">

											<label style="margin-left: 280px">Atas Nama  </label>
										</div>	
										 <div class="col-md-10 ol-sm-10">

											<h4 style="margin-left: 250px">SKY AIRLINES  </h4>
										</div>	
							
									</form>
								</div>
								<div class="paypal-pay"> 
									<div class="col-md-2 col-sm-2 col-xs-4">
									</div>
								</div>
							</div>
						</div>
					</div>
				<div class="col-md-4 col-sm-4 booking-sidebar">
						<div class="sidebar-item">
							<h4><i class="fa fa-phone"></i>Butuh Bantuan ?</h4>
							<div class="sidebar-body text-center">
								<p>Butuh Bantuan? Call Silahkan hubungi customer care.</p>
								<h2>+62 0896876543456</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END: BOOKING TAB -->

	<?php
include "page/footer.php";
	?>
<!-- END: SITE-WRAPPER -->

<!-- Load Scripts -->
<script src="assets/js/respond.js"></script>
<script src="assets/js/jquery.js"></script>
<script src="assets/plugins/owl.carousel.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>
<script src="assets/plugins/wow.min.js"></script>
<script src="assets/js/js.js"></script>
</body>

<!-- Mirrored from limpidthemesdemo.com/Themeforest/html/cruise-demo/light/flight-booking.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Feb 2019 12:40:17 GMT -->
</html>
<?php  } } else{ include 'login.php';}?>
