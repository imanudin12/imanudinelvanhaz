<?php
error_reporting(0);

@session_start();
if ($_SESSION['id_penumpang']){
	include 'private/lib/function.php';
	include "koneksi.php";
	$idletime =70 * 70; 

	if(isset($_SESSION["timestamp"])) {
		if (time()-$_SESSION["timestamp"]>$idletime){
			session_destroy();
			echo "<script>alert('Waktu Login Anda Telah Habis !'); window.location = 'login.php'</script>";
		}
	}else{
		$_SESSION["timestamp"]=time();
	}

//pembuatan session timestamp $_SESSION["timestamp"]=time();
	?>


<?php
include "page/header.php";
?>
		</div>
	</div>
	
	<!-- START: BOOKING TAB -->
	<div class="row booking-tab">
		<div class="container clear-padding">
			<ul class="nav nav-tabs">
				<li class=" col-md-4 col-sm-4 col-xs-4"><a data-toggle="tab" href="#review-booking" class="text-center"> <span></span></a></li>
				<li class="active col-md-4 col-sm-4 col-xs-4"><a data-toggle="tab" href="#billing-info" class="text-center"><i class="fa fa-check-square"></i> <span>Transfer</span></a></li>
				<li class="col-md-4 col-sm-4 col-xs-4"><a data-toggle="tab" href="-info" class="text-center"></i> <span></span></a></li>
			</ul> 
		</div>
	</div>
	<div class="row booking-detail">
		<div class="container clear-padding">
			<div class="tab-content">
				<div id="review-booking" class="tab-pane fade">
					<div class="col-md-8 col-sm-8">
						<div class="flight-list-v2">
							<div class="flight-list-main">	
								<div class="col-md-2 col-sm-2 text-center airline">
									<img src="assets/images/airline/vistara-2x.png" alt="cruise">
									<h6>Vistara</h6>
								</div>
								<div class="col-md-3 col-sm-3 departure">
									<h3><i class="fa fa-plane"></i> LHR 19:00</h3>
									<h5 class="bold">SAT, 21 SEP</h5>
									<h5>London, UK</h5>
								</div>
								<div class="col-md-4 col-sm-4 stop-duration">
									<div class="flight-direction">
									</div>
									<div class="stop">
									</div>
									<div class="stop-box">
										<span>0 Stop</span>
									</div>
									<div class="duration text-center">
										<span><i class="fa fa-clock-o"></i> 02h 00m</span>
									</div>
								</div>
								<div class="col-md-3 col-sm-3 destination">
									<h3><i class="fa fa-plane fa-rotate-90"></i> DEL 21:00</h3>
									<h5 class="bold">SAT, 21 SEP</h5>
									<h5>New Delhi, IN</h5>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="flight-list-footer">
								<div class="col-md-6 col-sm-6 col-xs-6 sm-invisible">
									<span><i class="fa fa-plane"></i> Economy</span>
									<span class="refund"><i class="fa fa-undo"></i> Refundable</span>
									<span><i class="fa fa-suitcase"></i> 10 KG</span>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12 clear-padding">
									<div class="pull-right">
										<a href="#">Edit Flight</a>
									</div>
								</div>
							</div>
						</div>
						<div class="login-box">
							<h3>Sign In</h3>
							<div class="booking-form">
								<div class="col-md-6 col-sm-6">
									<form >
										<label>Email</label>
										<input class="form-control" type="email" name="emailid" placeholder="Enter Your Email" required>
										<label>Password</label>
										<input class="form-control" type="password" name="password" placeholder="Enter Password" required>
										<a href="#">Forget Password?</a>
										<label>Phone Number (Optional)</label>
										<input class="form-control" type="text" name="phone">
										<label><input type="checkbox" name="remember"> Remember me</label>
										<button type="submit">Login</button>
									</form>
								</div>
								<div class="col-md-6 col-sm-6 text-center">
									<div class="social-media-login">
										<a href="#"><i class="fa fa-facebook"></i>Log in With Facebook</a>
										<span>Or</span>
										<a href="#"><i class="fa fa-plus"></i>Create Account</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 booking-sidebar">
						<div class="sidebar-item">
							<h4><i class="fa fa-bookmark"></i>Fare Details</h4>
							<div class="sidebar-body">
								<table class="table">
									<tr>
										<td>Adult 1</td>
										<td>$199</td>
									</tr>
									<tr>
										<td>Base Fare</td>
										<td>$100</td>
									</tr>
									<tr>
										<td>Service Fee</td>
										<td>$50</td>
									</tr>
									<tr>
										<td>Airport Taxes</td>
										<td>$20</td>
									</tr>
									<tr>
										<td>You Pay</td>
										<td class="total">$500</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="sidebar-item contact-box">
							<h4><i class="fa fa-phone"></i>Butuh Bantuan?</h4>
							<div class="sidebar-body text-center">
								<p>Butuh Bantuan ? Silahkan hubungi admin</p>
								<h2>+91 1234567890</h2>
							</div>
						</div>
					</div>
				</div>
				<div id="passenger-info" class="tab-pane fade">
					<div class="col-md-8 col-sm-8">
						<div class="passenger-detail">
							<h3>Passenger Details</h3>
							<div class="passenger-detail-body">
								<form >
									<div class="col-md-6 ol-sm-6">
										<label>First Name</label>
										<input type="text" name="firstname" required class="form-control">
									</div>
									<div class="col-md-6 ol-sm-6">
										<label>Last Name</label>
										<input type="text" name="lastname" required class="form-control">
									</div>
									<div class="col-md-6 ol-sm-6">
										<label>Email</label>
										<input type="email" name="email" required class="form-control">
									</div>
									<div class="col-md-6 ol-sm-6">
										<label>Verify Email</label>
										<input type="email" name="verify_email" class="form-control">
									</div>
									<div class="col-md-6 ol-sm-6">
										<label>Country Code</label>
										<select name="countrycode" class="form-control">
											<option>+1 United States</option>
											<option>+1 Canada</option>
											<option>+44 United Kingdom</option>
											<option>+91 India</option>
										</select>
									</div>
									<div class="col-md-6 ol-sm-6">
										<label>Phone Number</label>
										<input type="text" name="phonenumber" class="form-control" required>
									</div>
									<div class="col-md-12">
										<label><input type="checkbox" name="alert"> Send me newsletters and updates</label>
									</div>
									<div class="text-center">
										<button type="submit">CONTINUE <i class="fa fa-chevron-right"></i></button>
									</div>
								</form>
							</div>
						</div>
					</div>
				<div class="sidebar-item contact-box">
							<h4><i class="fa fa-phone"></i>Butuh Bantuan?</h4>
							<div class="sidebar-body text-center">
								<p>Butuh Bantuan ? Silahkan hubungi admin</p>
								<h2>+91 1234567890</h2>
							</div>
						</div>
					</div>
				</div>
<?php
	include 'konek.php';
	$id_pemesanan = $_GET['id_pemesanan'];
	$query = $conn->query("SELECT * FROM pemesanan WHERE id_pemesanan = $id_pemesanan");
	$data = mysqli_fetch_assoc($query);
?>


				<div id="billing-info" class="tab-pane fade in active">
					<div class="col-md-8 col-sm-8">
						<div class="passenger-detail">
							<h3>Bukti Pembayaran</h3>
							<div class="passenger-detail-body">
								
									<h4>Add New Card</h4>
<form action="update.php?id_pemesanan=<?php echo $id_pemesanan; ?>"  method="post" enctype="multipart/form-data" id="form_ubah_menu">										<div class="col-md-6 ol-sm-6">
											<label>Card Number</label>
											<input type="text" name="kode_pemesanan" value="<?php echo  $data['kode_pemesanan']; ?>" required class="form-control" readonly>
										</div>
										<div class="col-md-6 ol-sm-6">
											<label>Nama Pengirim</label>
											<input type="text" name="nama_pengirim" required class="form-control">
										</div>
										<div class="col-md-6 ol-sm-6">
											<label>No Rekening</label>
											<input type="text" name="no_rekening" required class="form-control">
										</div>
										<div style="margin-bottom: 0px" class="col-md-6 ol-sm-6">
											<label>Jumlah Uang</label>
											<input type="text" name="jumlah_uang" class="form-control">
										</div>

										<div style="" class="col-md-6 ol-sm-6">
											<label>Gambar</label>
											<input type="file" name="image" class="form-control">
                          <input  style="margin-left: " type="checkbox" class="filled-in" id="filled-in-box" name="ubah_image" />
                          <label for="filled-in-box"> Ceklis Jika Memasukan Gambar</label>
</div>
										

								
									<div class="col-md-10 col-sm-10 col-xs-8">
											<button type="submit">CONFIRM BOOKING <i class="fa fa-chevron-right"></i></button>
									</form>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 booking-sidebar">
						<div class="sidebar-item">
							<h4><i class="fa fa-phone"></i>Need Help?</h4>
							<div class="sidebar-body text-center">
								<p>Need Help? Call us or drop a message. Our agents will be in touch shortly.</p>
								<h2>+91 1234567890</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
include "page/footer.php";
	?>
<!-- END: SITE-WRAPPER -->

<!-- Load Scripts -->
<script src="assets/js/respond.js"></script>
<script src="assets/js/jquery.js"></script>
<script src="assets/plugins/owl.carousel.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>
<script src="assets/plugins/wow.min.js"></script>
<script src="assets/js/js.js"></script>
</body>

<!-- Mirrored from limpidthemesdemo.com/Themeforest/html/cruise-demo/light/flight-booking.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Feb 2019 12:40:17 GMT -->
</html>
<?php  }  else{ include 'login.php';}?>
