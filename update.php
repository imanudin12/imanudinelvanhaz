<?php
// Load file koneksi.php
include "konek.php";

// Ambil data NIS yang dikirim oleh form_ubah.php melalui URL
$id_pemesanan = $_GET['id_pemesanan'];

// Ambil Data yang Dikirim dari Form
  $nama_pengirim        = $_POST['nama_pengirim'];
    $kode_pemesanan        = $_POST['kode_pemesanan'];

  $no_rekening  = $_POST['no_rekening'];
  $jumlah_uang  = $_POST['jumlah_uang'];

// Cek apakah user ingin mengubah imagenya atau tidak
if(isset($_POST['ubah_image'])){ // Jika user menceklis checkbox yang ada di form ubah, lakukan :
  // Ambil data image yang dipilih dari form
  $image = $_FILES['image']['name'];
  $_FILES['image']['tmp_name'];
  
  // Set path folder tempat menyimpan imagenya
  $path = "assets/images/";

  // Rename nama imagenya dengan menambahkan tanggal dan jam upload
  $imagebaru = $path . basename($_FILES['image']['name']);

  // Proses upload
  if(move_uploaded_file($_FILES['image']['tmp_name'], $imagebaru)){ // Cek apakah gambar berhasil diupload atau tidak
    // Query untuk menampilkan data siswa berdasarkan NIS yang dikirim
    $query = "SELECT * FROM pemesanan WHERE id_pemesanan='".$id_pemesanan."'";
    $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
    $data = mysqli_fetch_array($sql); // Ambil data dari hasil eksekusi $sql

    // Cek apakah file image sebelumnya ada di folder images
    if(is_file("assets/images/".$data['image'])) // Jika image ada
      unlink("assets/images/".$data['image']); // Hapus file image sebelumnya yang ada di folder images
    
    // Proses ubah data ke Database
    $query = "UPDATE pemesanan SET status='pending', kode_pemesanan='".$kode_pemesanan."', nama_pengirim='".$nama_pengirim."', no_rekening='".$no_rekening."', jumlah_uang='".$jumlah_uang."', image='".$imagebaru."' WHERE id_pemesanan='".$id_pemesanan."'";
    $sql = mysqli_query($conn, $query); // Eksekusi/ Jalankan query dari variabel $query

    if($sql){ // Cek jika proses simpan ke database sukses atau tidak
      // Jika Sukses, Lakukan :
  echo "<script>alert('Behasil, Silahkan tunggu konfirmasi admin!');document.location.href='user-profile.php'</script>/n";
      
    }else{
      // Jika Gagal, Lakukan :
  echo "<script>alert('Gagal!!!');document.location.href='pembayaran.php'</script>/n";
      
    }
  }else{
    // Jika gambar gagal diupload, Lakukan :
    echo "Maaf, Gambar Gagal Untuk Diupload.";
    echo "<br><a href='../edit-artikel'>Kembali Ke Form</a>";
  }
}else{ // Jika user tidak menceklis checkbox yang ada di form ubah, lakukan :
  // Proses ubah data ke Database
    $query ="UPDATE pemesanan SET status='pending', kode_pemesanan='".$kode_pemesanan."', nama_pengirim='".$nama_pengirim."', no_rekening='".$no_rekening."', jumlah_uang='".$jumlah_uang."', image='".$imagebaru."' WHERE id_pemesanan='".$id_pemesanan."'";
    $sql = mysqli_query($conn, $query); // Eksekusi/ Jalankan query dari variabel $query

  if($sql){ // Cek jika proses simpan ke database sukses atau tidak
    // Jika Sukses, Lakukan :
  echo "<script>alert('Behasil, Silahkan tunggu konfirmasi admin!');document.location.href='user-profile.php'</script>/n";
    
  }else{
    // Jika Gagal, Lakukan :
  echo "<script>alert('Gagal!!!');document.location.href='pembayaran.php'</script>/n";
    
  }
}
?>