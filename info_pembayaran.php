<?php
error_reporting(0);

@session_start();
if ($_SESSION['id_penumpang']){
	include 'private/lib/function.php';
	include "koneksi.php";
	$idletime =60 * 60; 

	if(isset($_SESSION["timestamp"])) {
		if (time()-$_SESSION["timestamp"]>$idletime){
			session_destroy();
			echo "<script>alert('Waktu Login Anda Telah Habis !'); window.location = 'login.php'</script>";
		}
	}else{
		$_SESSION["timestamp"]=time();
	}

//pembuatan session timestamp $_SESSION["timestamp"]=time();
	?>

<!DOCTYPE html>
<html class="load-full-screen">

<!-- Mirrored from limpidthemesdemo.com/Themeforest/html/cruise-demo/light/flight-booking.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Feb 2019 12:40:17 GMT -->

<head>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="LimpidThemes">
	
	<title>SKY AIRLINES</title>
	
    <!-- STYLES -->
	<link href="assets/css/animate.min.css" rel="stylesheet">
	<link href="assets/css/bootstrap-select.min.css" rel="stylesheet">
	<link href="assets/css/owl.carousel.css" rel="stylesheet">
	<link href="assets/css/owl-carousel-theme.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="assets/css/flexslider.css" rel="stylesheet" media="screen">
	<link href="assets/css/style.css" rel="stylesheet" media="screen">
	<!-- LIGHT -->
	<link rel="stylesheet" type="text/css" href="assets/css/dummy.html" id="select-style">
	<link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	
	<link href="assets/css/light.css" rel="stylesheet" media="screen">
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600' rel='stylesheet' type='text/css'>

</head>
<body class="load-full-screen">

<!-- BEGIN: PRELOADER -->
<div id="loader" class="load-full-screen">
	<div class="loading-animation">
		<span><i class="fa fa-plane"></i></span>
		<span><i class="fa fa-bed"></i></span>
		<span><i class="fa fa-ship"></i></span>
		<span><i class="fa fa-suitcase"></i></span>
	</div>
</div>
<!-- END: PRELOADER -->

<!-- BEGIN: COLOR SWITCHER -->
<div class="cswitcher">
	<div id="color-switcher">
		<h6>CHOOSE COLOR</h6>
		<ul>
			<li class="green" data-path="assets/css/color/green.css"></li>
			<li class="light-green" data-path="assets/css/color/light-green.css"></li>
			<li class="red" data-path="assets/css/dummy.html"></li>
			<li class="blue" data-path="assets/css/color/blue.css"></li>  
			<li class="brown" data-path="assets/css/color/brown.css"></li>
			<li class="purple" data-path="assets/css/color/purple.css"></li>
			<li class="orange" data-path="assets/css/color/orange.css"></li>
			<li class="yellow" data-path="assets/css/color/yellow.css"></li>
		</ul>
	</div>
</div>
<span id="stoggle"><i class="fa fa-cog"></i></span>
<!-- END: COLOR SWITCHER -->

<!-- BEGIN: SITE-WRAPPER -->
		<div class="site-wrapper">
			<div class="row header-top">
				<div class="container clear-padding">
					<div class="navbar-contact">
						<div class="col-md-7 col-sm-6 clear-padding">
							<a href="#" class="transition-effect"><i class="fa fa-phone"></i> (+91) 1234567890</a>
							<a href="#" class="transition-effect"><i class="fa fa-envelope-o"></i> support@email.com</a>
						</div>
						<div class="col-md-5 col-sm-6 clear-padding search-box">
							<div class="col-md-6 col-xs-5 clear-padding">
								<form >
									<div class="input-group">
									</div>
								</form>
							</div>
							<?php
							include 'konek.php';
							$username = $_SESSION['username'];
							$ambildata = mysqli_query($conn, "SELECT * FROM penumpang WHERE username = '$username' ");
							while ($data_user = mysqli_fetch_array($ambildata)) 
							{
								?>
								<div class="col-md-6 col-xs-7 clear-padding user-logged">
									<a href="#" class="transition-effect">
										Hi. <?php echo $data_user['username']; ?>
									</a>
									<?php 
								}
								?>
								<a href="logout.php" class="transition-effect">
									<i class="fa fa-sign-out"></i>Keluar
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="clearfix"></div>
			<div class="row light-menu">
				<div class="container clear-padding">
					<!-- BEGIN: HEADER -->
					<div class="navbar-wrapper">
						<div class="navbar navbar-default" role="navigation">
							<!-- BEGIN: NAV-CONTAINER -->
							<div class="nav-container">
								<div class="navbar-header">
									<!-- BEGIN: TOGGLE BUTTON (RESPONSIVE)-->
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>

									<!-- BEGIN: LOGO -->
									<a class="navbar-brand logo"  href="index.html"></a>

					<img src="assets/images/1809403231_d1a07679-33ff-4f26-812b-678bd704fda6.png" style="height: 50px; width: 70px;"  href="index.html"></a>

								</div>

								<!-- BEGIN: NAVIGATION -->       
								<div class="navbar-collapse collapse">
									<ul class="nav navbar-nav navbar-right">
										<li class="dropdown">
											<a class="" href="index-3.php" ><i class="fa fa-home"></i> HOME </a>

										</li>

									
										<li class="dropdown mega">
											<a  href="about-us.php"><i class="fa fa-file"></i> TENTANG </i></a>
											
											<div class="clearfix"></div>
										</li>
										
											
										<li class="dropdown mega">
											<a class="" href="contact-us.php" ><i class="fa fa-ship"></i> KONTAK </i></a>
											
										
									</ul>
								</div>
								<!-- END: NAVIGATION -->
							</div>
							<!--END: NAV-CONTAINER -->
						</div>
					</div>
					<!-- END: HEADER -->
				</div>
			</div>
	
	<!-- END: PAGE TITLE -->
	
	<!-- START: BOOKING TAB -->
	<div class="row booking-tab">
		<div class="container clear-padding">
			<ul class="nav nav-tabs">
				<li class=" col-md-4 col-sm-4 col-xs-4"><a data-toggle="tab" href="#review-booking" class="text-center"><i></i> <span></span></a></li>
				<li class="active col-md-4 col-sm-4 col-xs-4"><a data-toggle="tab" href="#billing-info" class="text-center"><i class="fa fa-male"></i> <span>Pembayaran</span></a></li>	
				<li class="col-md-4 col-sm-4 col-xs-4"><a data-toggle="tab" href="#billing-infos" class="text-center"><i></i> <span></span></a></li>
			</ul> 
		</div>
	</div>


<div class="row booking-detail">
	<div class="container clear-padding">
		<div class="tab-content">
			<div id="billing-info" class="tab-pane fade in active">
				<div class="col-md-8 col-sm-8">
					<div class="passenger-detail">
						<h3>Pilih Pembayaran </h3>
						<div class="passenger-detail-body">
						<div class="user-preference">
							<h4 data-toggle="collapse" data-target="#flight-preferencea" aria-expanded="false" aria-controls="flight-preference">
									BRI<span class="pull-right"><i class="fa fa-chevron-down"></i></span>
							</h4>

							<div class="collapse" id="flight-preferencea">
								<form method="post" action="pemesanan.php">
									<div class="col-md-7 col-sm-7">
										<div class="col-md-6 col-sm-6 text-center airline">

								<img name="gambar" style="width: 120px;" src="assets/images/Tidak berjudul.png" alt="cruise">
							</div>
									</div>

									<div class="col-md-7 col-sm-7">

		<label data-toggle="collapse" data-target="#saved-card-1"><input type="radio" name="card"> <span>BRI</span></label>
									</div>

									<div class="col-md-7 col-sm-7">
										<label><b>Atas Nama</b> : Sky Airlines</label>
									</div>


									<div class="col-md-6 col-sm-6">
										<label><b>No Rekening</b> : 07979786</label>
																				<button name="submit"  type="submit" >Pesan Sekarang!!<i class="fa fa-chevron-right"></i></button>

									</div>
									<div class="clearfix"></div>
								</form>
									<div class="col-md-12 text-center">
									</div>
							</div>
						</div>	
						<div class="user-preference">

							<h4 data-toggle="collapse" data-target="#flight-preference" aria-expanded="false" aria-controls="flight-preference">
									BCA <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
							</h4>
																						<form method="post" action="pemesanan2.php">

							<div class="collapse" id="flight-preference">
									<div class="col-md-7 col-sm-7">
										<div class="col-md-6 col-sm-6 text-center airline">
								<img style="width: 120px;" src="assets/images/1280px-BCA_logo.svg.png" alt="cruise">
							</div>
									</div>
																		<div class="col-md-7 col-sm-7">

												<label data-toggle="collapse" data-target="#saved-card-1"><input type="radio" name="card"> <span>BCA</span></label>
</div>
									<div class="col-md-7 col-sm-7">
										<label><b>Atas Nama</b> : Sky Airlines</label>
									</div>
									<div class="col-md-6 col-sm-6">
										<label><b>No Rekening</b> : 03456732</label>
																				<button name="submit"  type="submit" >Pesan Sekarang!!<i class="fa fa-chevron-right"></i></button>
									</div>
									<div class="clearfix"></div>
								</form>
							</div>
						</div>	
														<div class="user-preference">
							<h4 data-toggle="collapse" data-target="#flight-preferences" aria-expanded="false" aria-controls="flight-preference">
									MANDIRI <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
							</h4>

						<form method="post" action="pemesanan3.php">

							<div class="collapse" id="flight-preferences">
									<div class="col-md-7 col-sm-7">
										<div class="col-md-6 col-sm-6 text-center airline">
								<img name="gambar" style="width: 120px;" src="assets/images/							Bank_Mandiri_logo.svg.png
" alt="cruise">
							</div>
									</div>
<div class="col-md-7 col-sm-7">

												<label data-toggle="collapse" data-target="#saved-card-1"><input type="radio" name="card"> <span>MANDIRI</span></label>
</div>									<div class="col-md-7 col-sm-7">
										<label><b>Atas Nama</b> : Sky Airlines</label>
									</div>
									<div class="col-md-6 col-sm-6">
										<label><b>No Rekening</b> : 3456789</label>
									
																			<button name="submit"  type="submit" >Pesan Sekarang!!<i class="fa fa-chevron-right"></i></button>
	
									</div>
									<div class="clearfix"></div>
								</form>
							</div>
						</div>	
						<div class="user-preference">
							<h4 data-toggle="collapse" data-target="#flight-preferenced" aria-expanded="false" aria-controls="flight-preference">
									 Paypal <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
							</h4>
								<div class="collapse" id="flight-preferenced">
									<div class="col-md-7 col-sm-7">
										<div class="col-md-6 col-sm-6 text-center airline">
								<img name="gambar"  style="width: 120px;" src="assets/images/paypal-logo.png

" alt="cruise">
							</div>
									</div>
									<form method="POST" method="pemesanan4.php">
									<div class="col-md-7 col-sm-7">

												<label data-toggle="collapse" data-target="#saved-card-1"><input type="radio" name="card"> <span>PAYPAL</span></label>
</div>
									<div class="col-md-7 col-sm-7">
										<label><b>Atas Nama</b> : Sky Airlines</label>
									</div>
									<div class="col-md-6 col-sm-6">
										<label><b>No Rekening</b> : 855965467</label>
										
										<button name="submit"  type="submit" >Pesan Sekarang!!<i class="fa fa-chevron-right"></i></button>
									</div>


									<div class="clearfix"></div>
							</div>
						</div>	

															</form>

																<div>
										</div>

								</div>
							</div>
						</div>
					</div>
					
		<div class="col-md-4 col-sm-4 booking-sidebar">
						<div class="sidebar-item">
							<h4><i class="fa fa-bookmark"></i>Total Keseluruhan</h4>
							<div class="sidebar-body">

								<table class="table">
		
					
	<?php
				$id_pelanggan = $_SESSION['id_penumpang'];


							$ambildata = mysqli_query($conn, "SELECT * FROM pemesanan WHERE id_pelanggan='$id_pelanggan' AND  status='Proses' ");
							while ($id = mysqli_fetch_array($ambildata)) 
							{
								?>
								<div class="col-md-6 col-xs-7 clear-padding user-logged">
									<tr>
										<td>Kode Pemesanan</td>
										<td><?php echo  $id['kode_pemesanan']; ?></td>
									</tr>
									<tr>
										<td>Asuransi</td>
										<td>IDR 0</td>
									</tr>
									<tr>
										<td>Biaya Layanan</td>
										<td style="color: blue">GRATIS</td>
									</tr>
									<tr>
										<td>Total Harga</td>
										<td class="total">IDR <?php echo  $id['total_bayar']; ?></td>
									</tr>
									<?php 
								}
								?>

								</table>
							</div>
						</div>
						
						<div class="sidebar-item contact-box">
							<h4><i class="fa fa-phone"></i>Butuh Bantuan?</h4>
							<div class="sidebar-body text-center">
								<p>Butuh Bantuan ? Silahkan hubungi admin</p>
								<h2>+62 1234567890</h2>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END: BOOKING TAB -->
<?php 
include "page/footer.php";
?>

</div>
<!-- END: SITE-WRAPPER -->
<!--=================================================-->
        <audio id="klik" src="private/assets/audio/klik.mp3"></audio>
    <audio id="success" src="private/assets/audio/success.mp3"></audio>
    <audio id="error" src="private/assets/audio/error.wav"></audio>
        <script src="private/assets/js/sweetalert.js"></script>
       <script src="private/js/jquery.min.js"></script>
<script>
		$(document).ready(function(){
			$('#tambah').submit(function(e){
					e.preventDefault();
					$.ajax({
						url: $(this).attr('action'),
						method: $(this).attr('method'),
						data: new FormData(this),
						dataType: 'JSON',
						contentType: false,
						processData: false,
						success: function(data){
							if(data.hasil == true)
							{
							
								window.location.assign('flight-booking.php');
							}
							else{
								swal({
									title : 'Gagal',
									icon  : 'error',
									text  : data.pesan,
								});
							}
						}
					});
			});
		});
	</script>
<!-- Load Scripts -->
<script src="assets/js/respond.js"></script>
<script src="assets/js/jquery.js"></script>
<script src="assets/plugins/owl.carousel.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>
<script src="assets/plugins/wow.min.js"></script>
<script src="assets/js/js.js"></script>
</body>

<!-- Mirrored from limpidthemesdemo.com/Themeforest/html/cruise-demo/light/flight-booking.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Feb 2019 12:40:17 GMT -->
</html>

<?php  } else{ include 'login.php';}?>
