

CREATE TABLE `kode_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kursi` varchar(10) NOT NULL,
  `kode_pemesanan` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO kode_booking VALUES("1","K001","B001");
INSERT INTO kode_booking VALUES("2","K002","B002");



CREATE TABLE `kota` (
  `id_kota` int(11) NOT NULL AUTO_INCREMENT,
  `kota` varchar(50) NOT NULL,
  `status` enum('Aktif','Off') NOT NULL,
  PRIMARY KEY (`id_kota`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO kota VALUES("1","Jakarta(JKT)","Aktif");
INSERT INTO kota VALUES("2","nuhnu","Aktif");
INSERT INTO kota VALUES("3","BandungSA","Aktif");



CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(20) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","officer");
INSERT INTO level VALUES("2","admin");



CREATE TABLE `partner` (
  `id_partner` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kode` varchar(15) NOT NULL,
  `harga` varchar(200) NOT NULL,
  `status` enum('Aktif','Off') NOT NULL,
  `kursi_ekonomi` varchar(50) NOT NULL,
  `kursi_bisnis` varchar(50) NOT NULL,
  `kelas_pertama` varchar(50) NOT NULL,
  `deskripsi` varchar(50) NOT NULL,
  PRIMARY KEY (`id_partner`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO partner VALUES("3","../images-artikel/7.png","GarudaAirlines","Yhi323","5000","Aktif","2121","432432","321321","321312");



CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT,
  `kode_pemesanan` varchar(12) NOT NULL,
  `tanggal_pemesanan` varchar(20) NOT NULL,
  `tempat_pemesanan` varchar(20) NOT NULL,
  `id_pelanggan` int(12) NOT NULL,
  `kode_kursi` varchar(20) NOT NULL,
  `id_rute` int(13) NOT NULL,
  `tujuan` varchar(20) NOT NULL,
  `tanggal_berangkat` varchar(20) NOT NULL,
  `jam_cekin` varchar(20) NOT NULL,
  `jam_berangkat` varchar(20) NOT NULL,
  `total_bayar` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `id_petugas` int(14) NOT NULL,
  PRIMARY KEY (`id_pemesanan`),
  KEY `id_rute` (`id_rute`),
  KEY `id_pelanggan` (`id_pelanggan`),
  KEY `id_petugas` (`id_petugas`),
  KEY `id_rute_2` (`id_rute`),
  CONSTRAINT `pemesanan_ibfk_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `penumpang` (`id_penumpang`),
  CONSTRAINT `pemesanan_ibfk_3` FOREIGN KEY (`id_rute`) REFERENCES `rute` (`id_rute`),
  CONSTRAINT `pemesanan_ibfk_4` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

INSERT INTO pemesanan VALUES("15","K0002","04/03/2019","alamat ","257","K0001","98","Padang(PDGC)","04/03/2019","16:04","16:04","2,559,000","Proses","20");
INSERT INTO pemesanan VALUES("16","K0002","04/03/2019","alamat ","257","K0001","100","Padang(PDGC)","04/03/2019","18:30","18:30","5,400,000","Proses","20");
INSERT INTO pemesanan VALUES("17","K0003","04/03/2019","alamat ","257","K0002","98","Padang(PDGC)","04/03/2019","16:04","16:04","2,559,000","Proses","20");
INSERT INTO pemesanan VALUES("18","K0004","04/03/2019","alamat ","257","K0003","100","Padang(PDGC)","04/03/2019","18:30","18:30","5,400,000","Proses","20");
INSERT INTO pemesanan VALUES("19","K0005","04/03/2019","alamat ","257","K0004","98","Padang(PDGC)","04/03/2019","16:04","16:04","2,559,000","Proses","20");
INSERT INTO pemesanan VALUES("20","K0006","04/03/2019","alamat ","257","K0005","98","Padang(PDGC)","04/03/2019","16:04","16:04","2,559,000","Proses","20");
INSERT INTO pemesanan VALUES("21","K0007","04/03/2019","alamat ","257","K0006","98","Padang(PDGC)","04/03/2019","16:04","16:04","2,559,000","Proses","20");
INSERT INTO pemesanan VALUES("22","K0008","04/03/2019","alamat ","257","K0007","98","Padang(PDGC)","04/03/2019","16:04","16:04","2,559,000","Proses","20");



CREATE TABLE `penumpang` (
  `id_penumpang` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `password_confirm` varchar(200) NOT NULL,
  `alamat_penumpang` varchar(20) NOT NULL,
  `tanggal_lahir` varchar(20) NOT NULL,
  `jenis_kelamin` enum('Laki-Laki','Perempuan') NOT NULL,
  `telefone` char(13) NOT NULL,
  `batas_login` int(2) NOT NULL,
  `blokir` enum('Aktif','Blokir') NOT NULL,
  `id_session` varchar(100) NOT NULL,
  `level` enum('user') NOT NULL,
  PRIMARY KEY (`id_penumpang`)
) ENGINE=InnoDB AUTO_INCREMENT=266 DEFAULT CHARSET=latin1;

INSERT INTO penumpang VALUES("256","elvanhaz@gmail.com","Imanelvanhaz","beec73b9de2d7610134b888333dd5337","beec73b9de2d7610134b888333dd5337","sad","11/11/1111","Laki-Laki","0897675678976","0","Aktif","p9lomnt243dp2ie2e2u5as5ks2","user");
INSERT INTO penumpang VALUES("257","elvanhaz@gmail.com","ELvanhaz","beec73b9de2d7610134b888333dd5337","beec73b9de2d7610134b888333dd5337","alamat ","12/12/2001","Perempuan","9876543567890","0","Aktif","b747g8fs853pb3ur9iuu9da3r7","user");
INSERT INTO penumpang VALUES("265","elvanhazi@gmail.com","sad","beec73b9de2d7610134b888333dd5337","beec73b9de2d7610134b888333dd5337","sad","2019-03-20","Laki-Laki","6754345678","0","Aktif","","user");



CREATE TABLE `pesan_detail` (
  `id_detail` int(11) NOT NULL AUTO_INCREMENT,
  `dewasa` varchar(20) NOT NULL,
  `anak` varchar(10) NOT NULL,
  PRIMARY KEY (`id_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

INSERT INTO pesan_detail VALUES("1","2","2");
INSERT INTO pesan_detail VALUES("2","3","0");
INSERT INTO pesan_detail VALUES("3","3","0");
INSERT INTO pesan_detail VALUES("4","3","0");
INSERT INTO pesan_detail VALUES("5","3","0");
INSERT INTO pesan_detail VALUES("6","3","0");
INSERT INTO pesan_detail VALUES("7","3","0");
INSERT INTO pesan_detail VALUES("8","3","0");
INSERT INTO pesan_detail VALUES("9","3","0");
INSERT INTO pesan_detail VALUES("10","3","0");
INSERT INTO pesan_detail VALUES("11","3","0");
INSERT INTO pesan_detail VALUES("12","3","0");
INSERT INTO pesan_detail VALUES("13","3","0");
INSERT INTO pesan_detail VALUES("14","3","0");
INSERT INTO pesan_detail VALUES("15","3","0");
INSERT INTO pesan_detail VALUES("16","3","0");
INSERT INTO pesan_detail VALUES("17","3","0");
INSERT INTO pesan_detail VALUES("18","3","0");
INSERT INTO pesan_detail VALUES("19","3","0");
INSERT INTO pesan_detail VALUES("20","3","0");
INSERT INTO pesan_detail VALUES("21","3","0");
INSERT INTO pesan_detail VALUES("22","3","0");
INSERT INTO pesan_detail VALUES("23","3","0");



CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_petugas` varchar(20) NOT NULL,
  `no_telpon` text NOT NULL,
  `gambar_menu` varchar(200) NOT NULL,
  `status` enum('Active','Off') NOT NULL,
  `id_session` varchar(200) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`),
  CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("20","admin","elvanhazi@gmail.com","beec73b9de2d7610134b888333dd5337","admin","32132","../images-artikel/img108.jpg","Active","jrn0chfjnkoijgqjprk3mior20","2");
INSERT INTO petugas VALUES("31","Vanhaz","imanelvanhaz@gmail.com","e86e3f8b060fe4bf56763f1767e3588c","Elvanhaz","0987567890","../images-artikel/img107.jpg","Active","bccdcucuoaeiq0nbjpb91peul3","2");



CREATE TABLE `rute` (
  `id_rute` int(11) NOT NULL AUTO_INCREMENT,
  `tujuan` varchar(20) NOT NULL,
  `rute_awal` varchar(20) NOT NULL,
  `rute_akhir` varchar(20) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `pp` varchar(20) NOT NULL,
  `pergi` varchar(20) NOT NULL,
  `jam_berangkat` varchar(20) NOT NULL,
  `jam_pulang` varchar(50) NOT NULL,
  `durasi` varchar(20) NOT NULL,
  `dewasa` varchar(20) NOT NULL,
  `anak` varchar(20) NOT NULL,
  `id_type_transportasi` int(11) NOT NULL,
  `id_transportasi` int(11) NOT NULL,
  PRIMARY KEY (`id_rute`),
  KEY `id_transportasi` (`id_transportasi`),
  CONSTRAINT `rute_ibfk_1` FOREIGN KEY (`id_transportasi`) REFERENCES `transportasi` (`id_transportasi`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

INSERT INTO rute VALUES("1","Bandung","Bandung (BDS)","Jakarta(JKTC)","150000","0000-00-00","0000-00-00","12:00","13:00","1jam","3","0","1","1");
INSERT INTO rute VALUES("95","Jakarta  (JKTC)","Denpasar-Bali (DPSC)","bandung","1.084.000","04/04/2019","","14:45","20:15","6 jam","3","0","1","11");
INSERT INTO rute VALUES("96","Medan (MESC)","Yogjakarta (YOGC)","Medan (MESC)","2.073.000","Minggu 14/04/2019","","17:51","4:45","11 jam","3","0","1","11");
INSERT INTO rute VALUES("97","Yogyakarta (JOGC)","Padang (PDGC)","Yogyakarta (JOGC)","1.266.000","04/17/2019","Selasa 19/04/2019","20:55","7:38","11 jam ","3","0","2","11");
INSERT INTO rute VALUES("98","Surabaya (SUBC)","Surabaya (SUBC)","Padang(PDGC)","853000","04/03/2019","","16:04","1:07","9jam","3","0","1","12");
INSERT INTO rute VALUES("99","Bandung(BDO)","Denpasar-Bali (DPSC)","Bandung(BDO)","853.000","Sabtu 06/04/2019","","15:40","16:20","1jam","3","0","1","11");
INSERT INTO rute VALUES("100","Padang(PDGC)","Surabaya (SUBC)","Padang(PDGC)","1800000","04/03/2019","","18:30","22:10","4jam","3","0","43","12");



CREATE TABLE `timer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `waktu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO timer VALUES("1","60");



CREATE TABLE `transportasi` (
  `id_transportasi` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(20) NOT NULL,
  `keterangan` text NOT NULL,
  `logo` varchar(100) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `kursi_ekonomi` varchar(50) NOT NULL,
  `kursi_bisnis` varchar(50) NOT NULL,
  `kelas_pertama` varchar(50) NOT NULL,
  `status` enum('Aktif','Off') NOT NULL,
  `id_type_transportasi` int(11) NOT NULL,
  PRIMARY KEY (`id_transportasi`),
  KEY `id_type_transportasi` (`id_type_transportasi`),
  CONSTRAINT `transportasi_ibfk_1` FOREIGN KEY (`id_type_transportasi`) REFERENCES `type_transportasi` (`id_type_transportasi`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO transportasi VALUES("1","B001","Bagus","sad","sad","130","ds","ds","Aktif","1");
INSERT INTO transportasi VALUES("11","GI65Hd","Baik","../images-artikel/7.png","LionAir","120","40","25","Aktif","1");
INSERT INTO transportasi VALUES("12","67gfhR46","Baik","../images-artikel/Lambang Garuda Indonesia.png","GarudaAirlines","120","50","20","Aktif","1");



CREATE TABLE `type_transportasi` (
  `id_type_transportasi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_type` varchar(20) NOT NULL,
  `keterangan` varchar(20) NOT NULL,
  PRIMARY KEY (`id_type_transportasi`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO type_transportasi VALUES("1","One Way","Baik");
INSERT INTO type_transportasi VALUES("2","Two Way","Apajah");

