<?php
error_reporting(0);

@session_start();
if ($_SESSION['id_penumpang']){
  include 'private/lib/function.php';
  include "koneksi.php";
  $idletime =70 * 70; 

  if(isset($_SESSION["timestamp"])) {
    if (time()-$_SESSION["timestamp"]>$idletime){
      session_destroy();
      echo "<script>alert('Waktu Login Anda Telah Habis !'); window.location = 'login.php'</script>";
    }
  }else{
    $_SESSION["timestamp"]=time();
  }

//pembuatan session timestamp $_SESSION["timestamp"]=time();
  ?>


<?php
include "page/header.php";
?>
					</div>
					<!--END: NAV-CONTAINER -->
				</div>
			</div>
			<!-- END: HEADER -->
		</div>
	</div>
	<!-- START: USER PROFILE -->
	<div class="row user-profile">
		<div class="container">
			<div class="col-md-12 user-name">
          <?php
              include 'konek.php';
              $username = $_SESSION['username'];
              $ambildata = mysqli_query($conn, "SELECT * FROM penumpang WHERE username = '$username' ");
              while ($data_user = mysqli_fetch_array($ambildata)) 
              {
                ?>
				<h3>PEMESAN : <?php echo $data_user['username']; ?></h3>
        <?php 
                }
                ?>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="user-profile-tabs">
					<ul class="nav nav-tabs">
						
					
					</ul>
				</div>
			</div>
			<div class="col-md-10 col-sm-10">
				<div class="tab-content">
					<div id="profile-overview" class="tab-pane fade ">
						<div class="col-md-6">
							<div class="brief-info">
								<div class="col-md-2 col-sm-2 clear-padding">
									<img src="assets/images/user1.jpg" alt="cruise">
								</div>
								<div class="col-md-10 col-sm-10">
									<h3>Lenore</h3>
									<h5><i class="fa fa-envelope-o"></i>lenore@lenoremail.com</h5>
									<h5><i class="fa fa-phone"></i>+91 1234567890</h5>
									<h5><i class="fa fa-map-marker"></i>Burbank, USA</h5>
								</div>
								<div class="clearfix"></div>
								<div class="brief-info-footer">
									<a href="#"><i class="fa fa-edit"></i>Edit Profile</a>
									<a href="#"><i class="fa fa-plus"></i>Add Travel Preference</a>
								</div>
							</div>

							<div class="clearfix"></div>
							<div class="most-recent-booking">
								<h4>Recent Booking</h4>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-plane"></i>New York<i class="fa fa-long-arrow-right"></i>New Delhi</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-bed"></i>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-suitcase"></i>Wonderful Europe</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="failed"><i class="fa fa-times"></i>failed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-plane"></i>New York<i class="fa fa-long-arrow-right"></i>New Delhi</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-bed"></i>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-suitcase"></i>Wonderful Europe</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="failed"><i class="fa fa-times"></i>failed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-profile-offer">
								<h4>Offers For You</h4>
								<div class="offer-body">
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>20% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 20% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>30% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 30% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>10% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 10% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="user-notification">
									<h4>Notification</h4>
									<div class="notification-body">
										<div class="notification-entry">
											<p><i class="fa fa-plane"></i> Domestic Flights Starting from $199 <span class="pull-right">1m ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bed"></i> 20% Cashback on hotel booking <span class="pull-right">1h ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bolt"></i> 50% off on all items <span class="pull-right">08h ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-sun-o"></i> New Year special offer <span class="pull-right">1d ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-plane"></i> Domestic Flights Starting from $199 <span class="pull-right">1m ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bed"></i> 20% Cashback on hotel booking <span class="pull-right">1h ago</span></p>
										</div>
									</div>
							</div>
						</div>
					</div>
				
						<div class="clearfix"></div>
						<div class="col-md-12">
			  <?php
        $id_pelanggan = $_SESSION['id_penumpang'];



              $ambildata = mysqli_query($conn, "SELECT * FROM pemesanan art INNER JOIN penumpang sat JOIN rute ket  WHERE art.id_pelanggan=sat.id_penumpang AND art.id_rute=ket.id_rute AND id_pelanggan='$id_pelanggan' ");
              while ($id = mysqli_fetch_array($ambildata)) 
              {
                ?>
							<div class="item-entry">
								<a href="pembayaran.php?id_pemesanan=<?php echo $id['id_pemesanan']; ?>"> <span  >BAYAR SEKARANG</span></a>
								<div class="item-content">
									<div class="item-body">
                     <div class="col-md-2 col-sm-2 clear-padding text-center">
                      <img style="width: 80px" src="assets/images/1809403231_d1a07679-33ff-4f26-812b-678bd704fda6.png" alt="cruise">
                    </div>

										<div class="col-md-4 col-sm-4">
											<h4><?php echo $id['rute_awal']; ?><i class="fa fa-long-arrow-right"></i><?php echo $id['rute_akhir']; ?></h4>
											<p>Berangkat: <?php echo $id['jam_berangkat']; ?> </p>
											<p>Pulang: <?php echo $id['jam_pulang']; ?> </p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p ><i class="fa fa-check"></i><?php echo $id['status']; ?></p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Cancel</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Total:</strong>Rp.<?php echo $id['total_bayar']; ?> </p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						
								</div>
							</div>
					
							
					
					
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END: USER PROFILE -->
<!-- START: FOOTER -->
<?php
include "page/footer.php";
?>
<!-- END: FOOTER -->

</div>
<!-- END: SITE-WRAPPER -->


<!-- Load Scripts -->
<script src="assets/js/respond.js"></script>
<script src="assets/js/jquery.js"></script>
<script src="assets/plugins/owl.carousel.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>
<script src="assets/plugins/wow.min.js"></script>
<script src="assets/js/js.js"></script>
</body>

<!-- Mirrored from limpidthemesdemo.com/Themeforest/html/cruise-demo/light/user-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Feb 2019 12:45:06 GMT -->
</html>
<?php  } } else{ include 'login.php';}?>
